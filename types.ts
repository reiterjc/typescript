console.log("it works");

// return type = string
function returnMyName(): string {
    return "Justin"
}

// return type = void
function sayHello(): void {
    console.log('hello');
}

// argument types
function multiply(value1: number, value2: number): number {
    return value1 * value2;
}

// function type
let myMultiply: (val1: number, val2: number) => number;

// objects (types and property names are both enforced)
let userData: { name: string, age: number} = {
    name: "Justin",
    age: 37
};

// complex object
type Complex = {data: number[], output: (all: boolean) => number[]} 
let complex: Complex = {
    data: [1, 3.2, 10],
    output: function (all: boolean) : number[] {
        return this.data;
    }
};
let complex2: Complex = {
    data: [10, 30.2, 100],
    output: function (all: boolean) : number[] {
        return this.data;
    }
};

// union types
let myRealAge: string | number = 37;
myRealAge = "37";
// myRealAge = true;  // fails

// check types
let finalValue = "A string";
if (typeof finalValue == 'string') {
    console.log("I'm a string");
}

// never
function neverReturns(): never {
    throw new Error('some error');
}

// nullable types
let canBeNull: number | null = 12;  // "strictNullChecks": true,
canBeNull = null;
let canAlsoBeNull; // no strict check
canAlsoBeNull = null;
let canThisBeAny = null;  // null type infered, and can -only- be null.  not assumed to be type 'any'
// canThisBeAny = 12;  // fails

function controlMe(isTrue: boolean) {
    let result: number;
    if (isTrue) {
        result = 12;
    }
    // return result;  // fails because result may have not be initialize
}

/* ES 6 features */

// default values
const countdown = (start: number = 10): void => {
    while (start > 0) {
        start--;
    }
    console.log('Done!');
};

// spread operator
const numbers1 = [1, 10, 99, -5];
console.log(Math.max(...numbers1));

// rest operator
function makeArray(...args: number[]) {
    return args;
}
console.log(makeArray(3, 5, 2));

// desctructuring
const myHobbies = ["Cooking", "Sports"];
const [hobby1, hobby2] = myHobbies;
console.log(hobby1, hobby2);

const user = {userName: "Justin", age: 37};
const { userName: myName, age: myAge } = user;
console.log(myName, myAge);

// template literals
const userName = "Justin";
// const greeting = "Hello, I'm " + userName + ". It's nice to meet you.";
const greeting = `Hello, I'm ${userName}.  
It's nice to meet you.`;
console.log(greeting);

