class Person {
    name: string;
    private type: string;
    protected age: number;

    constructor(name: string, public username: string) {
        this.name = name;
    }

    printAge() {
        console.log(this.age);
    }

    setType(type: string) {
        this.type = type;
    }
}

const person = new Person("Bob", 'bob');
person.printAge();
person.setType("Cool guy");

// inheritance
class Justin extends Person {
    
    constructor(username: string) {
        super("Justin", username);
        this.age = 31;
    }
}

const justin = new Justin("justin");
console.log(justin);

// getters and setters
class Plant {
    private _species: string;

    get species() {
        return this._species;
    }

    set species(value: string) {
        if (value.length > 3) {
            this._species = value;
        } else {
            this._species = "Default";
        }
    } 
}

let plant = new Plant();
console.log(plant.species);
plant.species = "Green Plant";
console.log(plant.species);

// static properties and methods
class Helpers {
    static PI: number = 3.14;

    static calcCircumference(diameter: number) {
        return this.PI * diameter;
    }    
}
console.log(2 * Helpers.PI);
console.log(Helpers.calcCircumference(6));

// abstract classes
abstract class Project {
    projectName: string = "Default";
    budget: number;

    abstract changeName(name: string): void;

    calcBudget() {
        return this.budget * 2;
    }
}

class ITProject extends Project {

    changeName(name: string): void {
        this.projectName = name;
    }
}

// singleton and read-only properties
class OnlyOne {
    private static instance: OnlyOne;
    public readonly name: string;

    private constructor(name: string) {
        this.name = name;
    }

    static getInstance() {
        if (!OnlyOne.instance) {
            OnlyOne.instance = new OnlyOne("The only one");
        }
        return OnlyOne.instance;
    }
}

let onlyOne = OnlyOne.getInstance();

